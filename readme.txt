############ Begin Rabbit MQ  Setup ############
# Need to install rabbitmq first 
# If you have chocolatey just type
choco install rabbitmq

# Otherwise use the tutorial at this location
https://www.rabbitmq.com/install-windows.html#configure

# Rabbitmq should be running once you install it but if not use these options to kick it off
# Run RabbitMQ (On Windows) run this file
C:\Program Files\RabbitMQ Server\rabbitmq_server-3.8.6\sbin\rabbitmq-server.bat

# Launch rabbitmq server
rabbitmq-server

# Or this location
C:\Users\u0803160\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\RabbitMQ Server
############ End Rabbit MQ  Setup ############

# Install all other libraries with
pip install -r requirements.txt
# (Some of the packages overwrite the celery version with newer so it was put at the end)



# Commands to start terminals (myshop is the app name)

# Django terminal (check for migrations that need to be applied)
python manage.py runserver

### Celery ### terminal
celery -A myshop worker --pool=solo -l info

### Flower ### terminal
celery -A myshop flower

### Beat ### terminal
celery -A myshop beat -l INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler


# Once all the terminals are running you can access the flower dashboard via
http://localhost:5555

# You can see all the rest of your tasks etc. in the Django Admin terminal
http://localhost/Admin
create a user with 'python manage.py createsuperuser'