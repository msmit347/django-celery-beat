from __future__ import absolute_import, unicode_literals
from celery import shared_task
from django.core.mail import send_mail
import cx_Oracle
import pandas as pd
import subprocess

@shared_task
def update_fin_class():

    def query(script):
        data = pd.read_excel('G:/Data Sources/UIDPWD.xlsx', engine="openpyxl")
        UID = data.iloc[0,0]
        PWD = data.iloc[0,1]
        conn = cx_Oracle.connect(UID, PWD, "DWRAC_UMB_UUMG")
        df = pd.read_sql_query(script, conn)
        return df.to_string()

    script = str(''' SELECT DWID, DOMAIN, CODE, D_FINAN_CLASS_DESC, STATUS, SOURCE, LOAD_DATE 
                        FROM VOCAB.D_FINAN_CLASS''')
    return query(script)



@shared_task
def add(x, y):
    return x + y 
    
    
@shared_task
def run_report(filepath1):
    filepath2 = ''# get from venv/delete.py file
    filepath1 = ''# get from venv/delete.py file
    p1 = subprocess.run([filepath2, filepath1])
    return p1